package ab3;

/**
 * Stellt einen generischen Knoten des Baumes dar, welcher einen Wert (ungleich
 * null) speichert. Der Konten verwaltet selbstständig left-, right- sowie
 * parent-Referenzen und hält auch die die Höhe der Teilbäume sowie die Anzahl
 * der Elemente in Teilbäumen am aktuellen Stand.
 * 
 * @author Raphael Wigoutschnigg, Judith Michael, Peter Schartner
 * 
 */
public class Node {
	/**
	 * Erzeugt einen Knoten mit dem Wert value. Der Wert value kann nicht mehr
	 * geändert werden, weil dadurch die Konsistenze eines Baumes verletzt
	 * werden könnte.
	 * 
	 * @param value
	 *            Wert des Knotens (read-only)
	 * @throws IllegalArgumentException
	 *             Falls value == null ist
	 */
	public Node(Integer value) throws IllegalArgumentException {
        this.value = value;
	}
	/*
	public Node(Node copyNode) {
		this.value = copyNode.value;
		this.parent = copyNode.parent;
		this.left = copyNode.left;
		this.right = copyNode.right;
	}
	*/

	/**
	 * Linkes Kind des Knotens
	 */
	private Node left;

	/**
	 * rechtes Kind des Knotens
	 */
	private Node right;

	/**
	 * Elternknoten
	 */
	private Node parent;

	/**
	 * Wert des Knotens
	 */
	private Integer value;

	/**
	 * Höhe des Subbaumes dieses Knotens
	 */
	private int heightOfSubtree = 0;

	/**
	 * Gewicht des Subbaums dieses Knotens (dh Anzahl der Knoten)
	 */
	private int sizeOfSubtree = 1;

	/**
	 * Liefert das Gewicht des Subbaumen, dh die Anzahl der Knoten des Subbaums
	 * 
	 * @return Gewicht des Subbaums
	 */
	public int getSizeOfSubtree() {
		return sizeOfSubtree;
	}

	/**
	 * Liefert das linke Kind des Knotens
	 * 
	 * @return linkes Kind
	 */
	public Node getLeft() {
		return left;
	}

	/**
	 * Setzt das linke Kind des Knotens
	 * 
	 * @param left
	 *            linkes Kind
	 */
	public void setLeft(Node left) {
		// Zuerst vom alten "left" den Parent löschen
		if (this.left != null)
			this.left.setParent(null);
		this.left = left;
		if (left != null) {
			Node oldparent = left.parent;
			left.setParent(this);

			if (oldparent != null)
				oldparent.updateHeightAndWeight();
		}

		this.updateHeightAndWeight();
	}

	/**
	 * Liefert das rechte Kind des Knotens
	 * 
	 * @return rechtes Kind
	 */
	public Node getRight() {
		return right;
	}

	/**
	 * Setzt das rechte Kind des Knotens
	 * 
	 * @param right
	 *            rechtes Kind
	 */
	public void setRight(Node right) {
		// Zuerst vom alten "right" den Parent löschen
		if (this.right != null) {
			this.right.setParent(null);
		}

		this.right = right;
		if (right != null) {
			Node oldparent = right.parent;
			right.setParent(this);

			if (oldparent != null)
				oldparent.updateHeightAndWeight();
		}

		this.updateHeightAndWeight();
	}

	/**
	 * Liefert den Elternknoten
	 * 
	 * @return Elternknoten
	 */
	public Node getParent() {
		return parent;
	}

	/**
	 * Setzt den Elternknoten
	 * 
	 * @param parent
	 *            Elternknoten
	 */
	public void setParent(Node parent) {
		this.parent = parent;
	}

	/**
	 * Liefert den Wert des Knotens
	 * 
	 * @return Wert des Knotens
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Liefert die Höhe des Subbaumes dieses Knotens
	 * 
	 * @return Höhe des Subbaums
	 */
	public int getHeightOfSubtree() {
		return heightOfSubtree;
	}

	/**
	 * Liefert die Höhendifferenz der beiden Teilbäume. Liefert nur dann
	 * korrekte Werte, wenn getHeightOfSubtree korrekte Werte liefert
	 * 
	 * @return Höhendifferenz der beiden Teilbäume
	 */
	public int getHeightDiff() {
		if (right == null && left == null)
			return 0;

		if (right != null && left == null)
			return right.getHeightOfSubtree() + 1;

		if (right == null && left != null)
			return -left.getHeightOfSubtree() - 1;

		if (right != null && left != null)
			return right.getHeightOfSubtree() - left.getHeightOfSubtree();

		return 0;

	}

	/**
	 * Bestimmt für {@link #heightOfSubtree} sowie {@link #weightOfSubtree} die
	 * korrekten Werte von dem aktuellen Knoten bis zum Root Knoten
	 */
	private void updateHeightAndWeight() {

		Node act = this;

		while (act != null) {
			act.heightOfSubtree = 0;
			act.sizeOfSubtree = 1;

			if (act.left != null) {
				act.heightOfSubtree = Math.max(act.heightOfSubtree, act.left.heightOfSubtree + 1);
				act.sizeOfSubtree += act.left.sizeOfSubtree;
			}
			if (act.right != null) {
				act.heightOfSubtree = Math.max(act.heightOfSubtree, act.right.heightOfSubtree + 1);
				act.sizeOfSubtree += act.right.sizeOfSubtree;
			}

			act = act.parent;
		}
	}
}