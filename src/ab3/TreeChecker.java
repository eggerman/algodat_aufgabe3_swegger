package ab3;

public interface TreeChecker {
	/**
	 * Prüft, ob der durch den Wurzelknoten angegebene Baum die Form eines Heaps hat sowie die Max-Heap-Bedingung erfüllt
	 * @param root Wurzel des Baumes
	 * @return true, wenn der Baum die Form eines Heaps hat
	 */
	public Boolean isHeap(Node root);
	
	/**
	 * Prüft, ob der durch den Wurzelknoten angegebene Baum sortiert ist.
	 * @param root Wurzel des Baumes
	 * @return true, wenn der Baum sortiert ist
	 */
	public Boolean isSorted(Node root);
}
