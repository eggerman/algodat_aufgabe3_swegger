package ab3.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import ab3.Node;
import ab3.Tree;
import ab3.TreeChecker;
import ab3.impl.BiermannEgger.TreeCheckerImpl;
import ab3.impl.BiermannEgger.TreeImpl;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TreeTest {
	private static final int TEST_SIZE = 50000;
	private static final Random rand = new Random(System.currentTimeMillis());

	private static Tree tree = new TreeImpl();
	private static TreeChecker treeChecker = new TreeCheckerImpl();

	// 0 Pts
	@Test
	public void testTree_0_Basic() {
		// Achtung: testClear liefert bei der ersten Implementierung von
		// AbstractSortedTreeImpl eine Exception. Fehler wurde am 24.05.
		// korrigiert.
		testClear(tree);

		// Duplikate nicht erlaubt
		tree.clear();
		tree.addValue(1);
		tree.addValue(1);
		Assert.assertEquals(1, tree.size());

		// Größe testen
		testSize(tree);

	}

	// 2 Pts
	@Test
	public void testTree_2_Ordnungen() {
		tree.clear();

		// Befüllen
		List<Integer> data = Arrays.asList(5, 6, 3, 4, 10, 9, 8, 1, 2, 7);
		data.forEach(tree::addValue);

		if (isAVLTree(tree)) {
			Assert.assertEquals(Arrays.asList(2, 1, 4, 3, 6, 8, 7, 10, 9, 5), tree.toLRW());
			Assert.assertEquals(Arrays.asList(10, 8, 6, 7, 9, 4, 2, 1, 3, 5), tree.toRLW());
			Assert.assertEquals(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10), tree.toLWR());
			Assert.assertEquals(Arrays.asList(10, 9, 8, 7, 6, 5, 4, 3, 2, 1), tree.toRWL());
			Assert.assertEquals(Arrays.asList(5, 3, 1, 2, 4, 9, 7, 6, 8, 10), tree.toWLR());
			Assert.assertEquals(Arrays.asList(5, 9, 10, 7, 8, 6, 3, 4, 1, 2), tree.toWRL());
		} else {
			Assert.assertEquals(Arrays.asList(2, 1, 4, 3, 7, 8, 9, 10, 6, 5), tree.toLRW());
			Assert.assertEquals(Arrays.asList(7, 8, 9, 10, 6, 4, 2, 1, 3, 5), tree.toRLW());
			Assert.assertEquals(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10), tree.toLWR());
			Assert.assertEquals(Arrays.asList(10, 9, 8, 7, 6, 5, 4, 3, 2, 1), tree.toRWL());
			Assert.assertEquals(Arrays.asList(5, 3, 1, 2, 4, 6, 10, 9, 8, 7), tree.toWLR());
			Assert.assertEquals(Arrays.asList(5, 6, 10, 9, 8, 7, 3, 4, 1, 2), tree.toWRL());
		}

	}

	// 1 Pts
	@Test
	public void testTree_1_SortedList() {
		tree.clear();

		List<Integer> data = getRandomData(TEST_SIZE);

		// Fügt jeden Wert in den Baum ein
		data.forEach(tree::addValue);

		// Äquivalent wären folgende Codezeilen
		// data.forEach(v -> tree.addValue(v));
		// ODER
		// for(int v:data)
		// tree.addValue(v);

		Collections.sort(data);

		Assert.assertEquals(data, tree.toSortedList());
	}

	// 1 Pts
	@Test
	public void testTree_1_Contains() {
		// Einfachen Baum verwenden
		testContainsSimple(tree);

		// Zufällig generierten Baum verwenden
		testContainsRandom(tree);

		// Die Rute :) - Test auf Stackoverflow bei falscher Implementierung
		testContainsSorted(tree);
	}

	// 1 Pts
	@Test
	public void testTree_1_AVL_Basic() {
		// Nur einfache Rotationen
		tree.clear();

		int[] data = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
		for (int v : data)
			tree.addValue(v);

		Assert.assertEquals(true, isAVLTree(tree));

		tree.clear();

		data = new int[] { 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
		for (int v : data)
			tree.addValue(v);

		Assert.assertEquals(true, isAVLTree(tree));

	}

	// 2 Pts
	@Test
	public void testTree_2_AVL_Complex() {
		// Baum mit Doppelrotationen
		tree.clear();

		List<Integer> data = Arrays.asList(5, 6, 3, 4, 10, 9, 8, 1, 2, 7);
		data.forEach(tree::addValue);

		Assert.assertEquals(true, isAVLTree(tree));

		tree.clear();

		data = Arrays.asList(10, 15, 12, 4, 8, 7, 3, 1, 13);
		data.forEach(tree::addValue);

		Assert.assertEquals(true, isAVLTree(tree));

		// Zufällig generierte Bäume

		tree.clear();

		data = getRandomData(TEST_SIZE);
		data.forEach(tree::addValue);

		Assert.assertEquals(true, isAVLTree(tree));

	}

	// 2 Pts
	@Test
	public void testTree_2_Speedtest() {
		// Baum mit 1..n Werten erzeugen. Zeit messen
		tree.clear();

		List<Integer> data = new ArrayList<>(TEST_SIZE);
		for (int i = 0; i < TEST_SIZE; i++)
			data.add(i);

		long start = System.currentTimeMillis();
		data.forEach(tree::addValue);
		long ende = System.currentTimeMillis();

		// Dauer maximal 1 Sekunde
		Assert.assertEquals(true, (ende - start) < 1000);
	}

	// 2 Pts
	@Test
	public void testTreeChecker_2_Heap() {
		Assert.assertEquals(false, treeChecker.isHeap(generateSortedRootedTree()));
		Assert.assertEquals(false, treeChecker.isHeap(generateUnSortedRootedTree()));
		Assert.assertEquals(false, treeChecker.isHeap(generateNoHeap1()));
		Assert.assertEquals(false, treeChecker.isHeap(generateNoHeap2()));
		Assert.assertEquals(false, treeChecker.isHeap(generateNoHeap3()));
		Assert.assertEquals(false, treeChecker.isHeap(generateNoHeap4()));
		
		Assert.assertEquals(true, treeChecker.isHeap(generateHeap1()));
		Assert.assertEquals(true, treeChecker.isHeap(generateHeap2()));
		Assert.assertEquals(true, treeChecker.isHeap(generateHeap3()));
		Assert.assertEquals(true, treeChecker.isHeap(generateHeap4()));
	}

	// 1 Pts
	@Test
	public void testTreeChecker_1_Sorted() {
		Assert.assertEquals(true, treeChecker.isSorted(generateSortedRootedTree()));
		
		Assert.assertEquals(false, treeChecker.isSorted(generateUnSortedRootedTree()));
	}
	

	private Node generateNoHeap1()
	{
		Node root = new Node(10);
		root.setRight(new Node(9));
		
		return root;
	}
	
	private Node generateNoHeap2()
	{
		Node root = new Node(9);
		root.setLeft(new Node(9));
		root.setRight(new Node(9));
		root.getLeft().setLeft(new Node(8));
		root.getLeft().setRight(new Node(8));
		root.getRight().setRight(new Node(8));
		
		return root;
	}
	
	private Node generateNoHeap3()
	{
		Node root = new Node(9);
		root.setLeft(new Node(9));
		root.setRight(new Node(9));
		root.getLeft().setLeft(new Node(8));
		root.getLeft().setRight(new Node(8));
		root.getRight().setLeft(new Node(10));
		
		return root;
	}
	
	private Node generateNoHeap4()
	{
		Node root = new Node(9);
		root.setLeft(new Node(9));
		root.setRight(new Node(9));
		root.getLeft().setLeft(new Node(8));
		root.getLeft().setRight(new Node(8));
		root.getLeft().getLeft().setLeft(new Node(7));
		
		return root;
	}
	
	private Node generateHeap1()
	{
		Node root = new Node(10);
		
		return root;
	}
	
	private Node generateHeap2()
	{
		Node root = new Node(10);
		root.setLeft(new Node(9));
		
		return root;
	}
	
	private Node generateHeap3()
	{
		Node root = new Node(10);
		root.setLeft(new Node(9));
		root.setRight(new Node(9));
		
		return root;
	}
	
	private Node generateHeap4()
	{
		Node root = new Node(9);
		root.setLeft(new Node(9));
		root.setRight(new Node(9));
		root.getLeft().setLeft(new Node(8));
		root.getLeft().setRight(new Node(8));
		root.getRight().setLeft(new Node(8));
		
		return root;
	}
	
	
	private Node generateSortedRootedTree()
	{
		Node root = new Node(10);
		root.setLeft(new Node(10));
		root.setRight(new Node(10));
		root.getLeft().setLeft(new Node(4));
		root.getLeft().getLeft().setLeft(new Node(2));
		root.getLeft().getLeft().getLeft().setLeft(new Node(1));
		root.getLeft().setRight(new Node(10));
		root.getRight().setRight(new Node(25));
		
		return root;
	}
	
	private Node generateUnSortedRootedTree()
	{
		Node root = new Node(1);
		root.setLeft(new Node(2));
		root.setRight(new Node(3));
		root.getLeft().setLeft(new Node(4));
		root.getLeft().getLeft().setLeft(new Node(5));
		root.getLeft().getLeft().getLeft().setLeft(new Node(0));
		root.getLeft().setRight(new Node(1));
		root.getRight().setLeft(new Node(2));
		
		return root;
	}

	private void testContainsSimple(Tree tree) {
		tree.clear();

		tree.addValue(1);
		tree.addValue(7);
		tree.addValue(6);
		tree.addValue(26);
		tree.addValue(11);
		tree.addValue(2);
		tree.addValue(20);
		tree.addValue(12);
		tree.addValue(3);

		Assert.assertEquals(true, tree.containsValue(20));
		Assert.assertEquals(true, tree.containsValue(3));
		Assert.assertEquals(false, tree.containsValue(4));
		Assert.assertEquals(false, tree.containsValue(22));
	}

	private void testContainsRandom(Tree tree) {
		tree.clear();

		List<Integer> data = getRandomData(TEST_SIZE);

		data.forEach(tree::addValue);

		for (int v : data)
			Assert.assertEquals(true, tree.containsValue(v));

		// erzeuge ein Set mit allen Schlüsseln aus data
		Set<Integer> s = new TreeSet<>(data);

		for (int i = 0; i < TEST_SIZE; i++) {
			int v = rand.nextInt();
			Assert.assertEquals(s.contains(v), tree.containsValue(v));
		}
	}

	private void testContainsSorted(Tree tree) {
		tree.clear();

		List<Integer> data = getRandomData(TEST_SIZE);
		Collections.sort(data);

		data.forEach(tree::addValue);

		for (int v : data)
			Assert.assertEquals(true, tree.containsValue(v));

		// erzeuge ein Set mit allen Schlüsseln aus data
		Set<Integer> s = new TreeSet<>(data);

		for (int i = 0; i < TEST_SIZE; i++) {
			int v = rand.nextInt();
			Assert.assertEquals(s.contains(v), tree.containsValue(v));
		}
	}

	private void testSize(Tree tree) {
		tree.clear();

		tree.addValue(1);
		tree.addValue(7);
		tree.addValue(6);
		tree.addValue(26);
		tree.addValue(11);
		tree.addValue(2);
		tree.addValue(20);
		tree.addValue(12);
		tree.addValue(3);

		Assert.assertEquals(9, tree.size());
	}

	private void testClear(Tree tree) {
		tree.clear();

		tree.addValue(1);
		tree.addValue(7);
		tree.addValue(6);
		tree.addValue(26);
		tree.addValue(11);
		tree.addValue(2);
		tree.addValue(20);
		tree.addValue(12);
		tree.addValue(3);

		tree.clear();

		Assert.assertEquals(Arrays.asList(), tree.toLRW());
		Assert.assertEquals(-1, tree.getHeight());
		Assert.assertEquals(0, tree.size());
	}

	private static boolean isAVLTree(Tree tree) {
		return isAVLTree(tree.getRoot());
	}

	private static boolean isAVLTree(Node node) {
		if (node == null)
			return true;

		int leftH = getHeightOfSubtree(node.getLeft());
		int rightH = getHeightOfSubtree(node.getRight());

		if (Math.abs(leftH - rightH) > 1)
			return false;

		return isAVLTree(node.getLeft()) && isAVLTree(node.getRight());
	}

	private static int getHeightOfSubtree(Node node) {
		if (node == null)
			return -1;

		int leftH = getHeightOfSubtree(node.getLeft());
		int rightH = getHeightOfSubtree(node.getRight());

		return 1 + Math.max(leftH, rightH);
	}

	private List<Integer> getRandomData(int size) {
		ArrayList<Integer> data = new ArrayList<>(TEST_SIZE);

		for (int i = 0; i < TEST_SIZE; i++)
			data.add(rand.nextInt());
		return data;
	}
}