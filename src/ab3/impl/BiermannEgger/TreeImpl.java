package ab3.impl.BiermannEgger;

import java.util.List;

import ab3.AbstractSortedTreeImpl;

public class TreeImpl extends AbstractSortedTreeImpl {

	@Override
	public Boolean containsValue(Integer value) throws IllegalArgumentException {
		return null;
	}

	@Override
	public List<Integer> toRWL() {
		return null;
	}

	@Override
	public List<Integer> toLWR() {
		return null;
	}

	@Override
	public List<Integer> toLRW() {
		return null;
	}

	@Override
	public List<Integer> toRLW() {
		return null;
	}

	@Override
	public List<Integer> toWLR() {
		return null;
	}

	@Override
	public List<Integer> toWRL() {
		return null;
	}

	@Override
	public List<Integer> toSortedList() {
		return null;
	}

	@Override
	public boolean addValue(Integer value) throws IllegalArgumentException {
		// Hinweis: Löschen Sie die folgende Code-Zeile und implementieren Sie
		// die AVL-Einfüge-Operation. Sie können auf die Variable root von
		// AbstractSortedTreeImpl zugreifen, um die Knoten des Baumes
		// entsprechend anzupassen. Die Methode addValue der
		// AbstractSortedTreeImpl kann Ihnen helfen, den Umgang mit der Variable
		// root besser zu verstehen.
		return super.addValue(value);
	}

	@Override
	public void clear() {
		super.clear();
	}
}