package ab3;

import java.util.List;

/**
 * Schnittstelle, welche einen sortierten binären Wurzelnbaum (kurz Binärbaum)
 * darstellt. Null ist als Wert nicht erlaubt (IllegalArgumentException). Der
 * Baum darf keine doppelten Werte enthalten. Verwenden Sie zur Bestimmung der
 * Ordnung der Elemente die .compareTo-Methode.
 * 
 * @author Raphael Wigoutschnigg, Judith Michael, Peter Schartner
 */
public interface Tree {

	/**
	 * Liefert die Höhe des Baums zurück. Die Höhe ist um eines geringer als die
	 * Anzahl der Ebenen. Besitzt der Baum keine Elemente, so soll -1 zurück gegeben werden.
	 * 
	 * @return Höhe des Baumes
	 */
	public int getHeight();
	
	/**
	 * Liefert die Wurzel
	 * 
	 * @return Wurzel des Baumes
	 */
	public Node getRoot();
	
	/**
	 * Gibt die Anzahl der gespeicherten Werte zurück
	 * 
	 * @return Anzahl der Werte des Baumes
	 */
	public int size();

	/**
	 * Fügt einen Wert in den Baum ein.
	 * 
	 * @param value
	 *            Wert, der eingefügt werden soll
	 * @return true, wenn das Einfügen geklappt hat. False, wenn der Wert
	 *         bereits im Baum vorhanden war.
	 * @throws IllegalArgumentException
	 *             Falls value == null
	 */
	public boolean addValue(Integer value) throws IllegalArgumentException;

	/**
	 * Liefert true, wenn der Wert bereits im Baum vorhanden ist
	 * 
	 * @param value
	 *            zu überprüfender Wert
	 * @return true, wenn der Wert im Baum vorhanden ist
	 * @throws IllegalArgumentException
	 *             Fall value == null
	 */
	public Boolean containsValue(Integer value) throws IllegalArgumentException;

	/**
	 * Entfernt alle Knoten aus dem Baum
	 */
	public void clear();

	/**
	 * Gibt die RWL-Ordnung zurück
	 * 
	 * @return RWL-Ordnung
	 */
	public List<Integer> toRWL();

	/**
	 * Gibt die LWR-Ordnung zu zurück
	 * 
	 * @return LWR-Ordnung
	 */
	public List<Integer> toLWR();

	/**
	 * Gibt die LRW-Ordnung zu zurück
	 * 
	 * @return LRW-Ordnung
	 */
	public List<Integer>toLRW();

	/**
	 * Gibt die RLW-Ordnung zu zurück
	 * 
	 * @return RLW-Ordnung
	 */
	public List<Integer> toRLW();

	/**
	 * Gibt die WLR-Ordnung zu zurück
	 * 
	 * @return WLR-Ordnung
	 */
	public List<Integer> toWLR();

	/**
	 * Gibt die WRL-Ordnung zu zurück
	 * 
	 * @return WRL-Ordnung
	 */
	public List<Integer> toWRL();
	
	/**
	 * Liefert die Werte des Baumes sortiert zurück
	 * @return sortierte Liste der Werte des Baums
	 */
	public List<Integer> toSortedList();
}