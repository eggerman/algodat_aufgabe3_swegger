package ab3;

import ab3.Node;
import ab3.Tree;

public abstract class AbstractSortedTreeImpl implements Tree {

	protected Node root = null;

	@Override
	public int getHeight() {
		if(root == null)
			return -1;
		return root.getHeightOfSubtree();
	}

	@Override
	public int size() {
		if(root == null)
			return 0;
		return root.getSizeOfSubtree();
	}

	@Override
	public boolean addValue(Integer value) throws IllegalArgumentException {
		if (root == null) {
			root = new Node(value);
			return true;
		}

		Node actNode = root;

		boolean posFound = false;
		while (!posFound) {
			if (value.equals(actNode.getValue()))
				return false;

			if (value.compareTo(actNode.getValue()) < 0) {
				if (actNode.getLeft() == null) {
					posFound = true;
					actNode.setLeft(new Node(value));
				} else
					actNode = actNode.getLeft();
			} else {
				if (actNode.getRight() == null) {
					posFound = true;
					actNode.setRight(new Node(value));
				} else
					actNode = actNode.getRight();
			}
		}

		RestoreAVL(actNode);

		return true;
	}

	@Override
	public void clear() {
		root = null;
	}
	
	@Override
	public Node getRoot() {
		return root;
	}

	private void RestoreAVL(Node actNode) {
	    int heightDiff = actNode.getHeightDiff();

	    while (actNode.getParent() != null) {

            while (heightDiff * heightDiff < 2) {

                actNode = actNode.getParent();

                if(actNode == null)
                    break;

                heightDiff = actNode.getHeightDiff();
            }


            //Rotate Left
            if (heightDiff > 1) {

                //Check and perform Double Rotation
                if(actNode.getRight().getHeightDiff() < 0) {
                    actNode = actNode.getRight();
                    actNode = RotRight(actNode);
                }

                actNode = actNode.getParent();
                actNode = RotLeft(actNode);
            }
            //Rotate Right
            else if (heightDiff < 1) {

                //Check and perform Double Rotation
                if(actNode.getLeft().getHeightDiff() < 0) {
                    actNode = actNode.getLeft();
                    actNode = RotLeft(actNode);
                }

                actNode = actNode.getParent();
                actNode = RotLeft(actNode);
            }

            heightDiff = 0;
            if (actNode == null)
                break;
        }
        if (actNode != null)
            root = actNode;
	}

	private Node RotRight(Node rotPoint) {
        Node parent = new Node(0);
	    //Check if rotPoint has parent, if so set new child
        if(rotPoint.getParent() != null) {
            parent = new Node(rotPoint.getParent().getValue());
            parent.setLeft(rotPoint.getParent().getLeft());
            rotPoint.getParent().setRight(rotPoint.getLeft());
            parent.setRight(rotPoint.getLeft());
        }

        //Create the (new) Right SubTree for later insert
        Node RightSubtree = new Node(rotPoint.getValue());
        RightSubtree.setLeft(rotPoint.getLeft());
        RightSubtree.setRight(rotPoint.getRight());

	    //Rotate new rotPoint up and remove parent reference
        rotPoint = rotPoint.getLeft();
	    rotPoint.getParent().setLeft(null);

	    //Umhaengen
	    RightSubtree.setLeft(rotPoint.getRight());
	    rotPoint.setRight(RightSubtree);

	    //Reset parent
        if (parent.getValue() != 0)
            rotPoint.setParent(parent);

        return rotPoint;
    }
    private Node RotLeft(Node rotPoint) {

        Node parent = new Node(0);
        //Check if rotPoint has parent, if so set new child
        if(rotPoint.getParent() != null) {
            parent = new Node(rotPoint.getParent().getValue());
            parent.setRight(rotPoint.getParent().getRight());
            rotPoint.getParent().setLeft(rotPoint.getRight());
        }

        //Create the (new) Right SubTree for later insert
        Node LeftSubtree = new Node(rotPoint.getValue());
        LeftSubtree.setRight(rotPoint.getRight());
        LeftSubtree.setLeft(rotPoint.getLeft());

        //Rotate new rotPoint up and remove parent reference
        rotPoint = rotPoint.getRight();
        rotPoint.getParent().setRight(null);

        //Umhaengen
        LeftSubtree.setRight(rotPoint.getLeft());
        rotPoint.setLeft(LeftSubtree);

        //Reset parent
        if (parent.getValue() != 0)
            rotPoint.setParent(parent);

        return rotPoint;
    }
}
